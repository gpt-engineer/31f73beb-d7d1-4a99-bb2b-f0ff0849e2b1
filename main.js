function reply() {
    console.log("Reply button clicked");
    this.classList.toggle('clicked');
}

function retweet() {
    console.log("Retweet button clicked");
    this.classList.toggle('clicked');
}

function like() {
    console.log("Like button clicked");
    this.classList.toggle('clicked');
}

function share() {
    console.log("Share button clicked");
    this.classList.toggle('clicked');
}